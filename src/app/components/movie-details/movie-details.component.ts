import { Component, OnInit } from '@angular/core';
import {IMovieDetails} from '../../interfaces/movie-details.interface';
import {ActivatedRoute} from '@angular/router';
import {MoviesApiService} from '../../services/movies-api.service';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';
import {MatDialog} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  public finished = false;
  public movie: IMovieDetails = null;
  constructor(private route: ActivatedRoute, private moviesApi: MoviesApiService, private dialog: MatDialog, private sanitizer: DomSanitizer) {}

  ngOnInit() {
    const slug = this.route.snapshot.paramMap.get('slug');
    this.moviesApi.getMovieDetails(slug).subscribe(
      (response: IMovieDetails) => {
        this.finished = true;
        this.movie = response;
      },
      (err: any) => {
        this.finished = true;
        this.dialog.open(CustomDialogComponent, {
          data: {
            title: 'Error',
            message: 'Please try again'
          }
        });
      }
    );
  }

  public sanitizeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
