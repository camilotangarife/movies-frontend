import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MoviesApiService} from '../../services/movies-api.service';
import {ISignUpRequest} from '../../interfaces/sign-up-request.interface';
import {ISignUpResponse} from '../../interfaces/sign-up-response.interface';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public signupFormGroup: FormGroup;
  constructor(private moviesApi: MoviesApiService, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.signupFormGroup = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      password2: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      lastname: new FormControl('', [Validators.required]),
      birthdate: new FormControl(new Date(), [Validators.required]),
      address: new FormControl('', [Validators.required]),
    });
  }

  public arePasswordsTheSame(): boolean {
    if(this.signupFormGroup) {
      const v = this.signupFormGroup.value;
      return v.password === v.password2;
    }
    return false;
  }

  public hasError(controlName: string, errorName: string) {
    return this.signupFormGroup.controls[controlName].hasError(errorName);
  }

  public signup() {
    if (this.signupFormGroup.valid) {
      const formValue: ISignUpRequest = this.signupFormGroup.value;
      if (formValue.password === formValue.password2) {
        this.moviesApi.signUp(formValue)
          .subscribe(
            (response: ISignUpResponse) => {
              const dialogRef = this.dialog.open(CustomDialogComponent, {
                data: {
                  title: 'Your account was created succesfully',
                  message: 'Now you can log in'
                }
              });
              dialogRef.afterClosed().subscribe(result => {
                this.router.navigate(['/login']);
              });
            },
            (err: any) => {
              this.dialog.open(CustomDialogComponent, {
                data: {
                  title: 'Error',
                  message: err.error.error
                }
              });
            });
      }
    }
  }

}
