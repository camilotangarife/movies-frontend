import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MoviesApiService} from '../../services/movies-api.service';
import {MatDialog} from '@angular/material';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginFormGroup: FormGroup;

  constructor(private moviesApi: MoviesApiService, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.loginFormGroup = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }

  public hasError(controlName: string, errorName: string) {
    return this.loginFormGroup.controls[controlName].hasError(errorName);
  }

  public login() {
    if (this.loginFormGroup.valid) {
      const formValue = this.loginFormGroup.value;
      let title = 'Incorrect credentials';
      let message = 'Please try again';

      this.moviesApi.logIn(formValue.email, formValue.password)
        .subscribe((response: boolean) => {
          if (response) {
            title = 'Login succeed';
            message = '';
          }
          const dialogRef = this.dialog.open(CustomDialogComponent, {
            data: {
              title,
              message
            }
          });
          if (response) {
            dialogRef.afterClosed().subscribe(result => {
              this.router.navigate(['/suggested-movies']);
            });
          }
        }, err => {
          this.dialog.open(CustomDialogComponent, {
            data: {
              title,
              message
            }
          });
        });
    }
  }
}
