import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IMoviePreview} from '../../interfaces/movie-preview.interface';

@Component({
  selector: 'app-movie-preview',
  templateUrl: './movie-preview.component.html',
  styleUrls: ['./movie-preview.component.css']
})
export class MoviePreviewComponent implements OnInit {
  @Input()
  public movie: IMoviePreview;

  @Input()
  public showDelete = false;

  @Output()
  public deleteMovie = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  public delete() {
    this.deleteMovie.emit(this.movie.slug);
  }

}
