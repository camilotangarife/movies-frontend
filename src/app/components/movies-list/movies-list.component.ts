import { Component, OnInit } from '@angular/core';
import {MatDialog, PageEvent} from '@angular/material';
import {IMoviePreview} from '../../interfaces/movie-preview.interface';
import {MoviesApiService} from '../../services/movies-api.service';
import {IGetMoviesResponse} from '../../interfaces/get-movies-response.interface';
import {CustomDialogComponent} from '../custom-dialog/custom-dialog.component';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  public page = 0;
  public totalMovies = 0;
  public finished = false;
  public movies: Array<IMoviePreview> = [];
  constructor(private moviesApi: MoviesApiService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getMovies();
  }

  private getMovies() {
    this.finished = false;
    this.movies = [];
    this.moviesApi.getMovies(this.page).subscribe((response: IGetMoviesResponse) => {
      this.movies = response.movies;
      this.totalMovies = response.total_movies;
      this.finished = true;
    }, err => {
      this.finished = true;
      this.finished = true;
      this.dialog.open(CustomDialogComponent, {
        data: {
          title: 'Error',
          message: 'Please try again'
        }
      });
    });
  }

  public pageChanged(pageEvent: PageEvent) {
    this.page = pageEvent.pageIndex;
    this.getMovies();
  }
}
