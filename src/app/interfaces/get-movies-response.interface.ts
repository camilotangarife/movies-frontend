import {IMoviePreview} from './movie-preview.interface';

export interface IGetMoviesResponse {
  movies: IMoviePreview[];
  total_movies: number;
}
