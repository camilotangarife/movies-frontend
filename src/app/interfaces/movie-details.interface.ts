export interface IMovieDetails {
  title: string;
  slug: string;
  summary: string;
  image_url: string;
  director: string;
  writers: string;
  stars: string;
  trailer: string;
  year: number;
}
