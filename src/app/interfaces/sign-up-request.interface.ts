export interface ISignUpRequest {
  email: string;
  password: string;
  password2: string;
  name: string;
  lastname: string;
  birthdate: Date;
  address: string;
}
