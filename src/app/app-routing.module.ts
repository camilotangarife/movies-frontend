import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {MoviesListComponent} from './components/movies-list/movies-list.component';
import {SignupComponent} from './components/signup/signup.component';
import {MovieDetailsComponent} from './components/movie-details/movie-details.component';
import {SuggestedMoviesComponent} from './components/suggested-movies/suggested-movies.component';
import {AuthGuard} from './guards/auth.guard';
import {NoAuthGuard} from './guards/no-auth.guard';
import {MyMoviesComponent} from './components/my-movies/my-movies.component';
import {CreateMovieComponent} from './components/create-movie/create-movie.component';

const routes: Routes = [
  {path: '', component: MoviesListComponent},
  {path: 'movies', redirectTo: '', pathMatch: 'full'},
  {path: 'suggested-movies', component: SuggestedMoviesComponent, canActivate: [AuthGuard]},
  {path: 'create-movie', component: CreateMovieComponent, canActivate: [AuthGuard]},
  {path: 'my-movies', component: MyMoviesComponent, canActivate: [AuthGuard]},
  {path: 'movies/:slug', component: MovieDetailsComponent},
  {path: 'login', component: LoginComponent, canActivate: [NoAuthGuard]},
  {path: 'signup', component: SignupComponent, canActivate: [NoAuthGuard]},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
