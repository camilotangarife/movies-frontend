import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {IGetMoviesResponse} from '../interfaces/get-movies-response.interface';
import {map} from 'rxjs/operators';
import {ISignUpResponse} from '../interfaces/sign-up-response.interface';
import {ISignUpRequest} from '../interfaces/sign-up-request.interface';
import {IMovieDetails} from '../interfaces/movie-details.interface';

@Injectable({
  providedIn: 'root'
})
export class MoviesApiService {
  private moviesPath = '/movies';
  private suggestedMoviesPath = '/suggested-movies';
  private myMoviesPath = '/my-movies';
  private deleteMoviePath = this.myMoviesPath + '/delete';
  private signUpPath = '/signup';
  private logInPath = '/login';
  private logOutPath = '/logout';

  constructor(private http: HttpClient) { }

  private get MOVIES_URL(): string {
    return environment.api_url + this.moviesPath;
  }

  private get SUGGESTED_MOVIES_URL(): string {
    return environment.api_url + this.suggestedMoviesPath;
  }

  private get MY_MOVIES_URL(): string {
    return environment.api_url + this.myMoviesPath;
  }

  private get DELETE_MOVIE_URL(): string {
    return environment.api_url + this.deleteMoviePath;
  }

  private get SIGN_UP_URL(): string {
    return environment.api_url + this.signUpPath;
  }

  private get LOG_IN_URL(): string {
    return environment.api_url + this.logInPath;
  }

  private get LOG_OUT_URL(): string {
    return environment.api_url + this.logOutPath;
  }

  private static saveToken(token: string) {
    localStorage.setItem('OmnMoviesToken', token);
  }

  public static getToken(): string {
    const token = localStorage.getItem('OmnMoviesToken');
    return token ? token : '';
  }

  public static deleteToken() {
    localStorage.removeItem('OmnMoviesToken');
  }

  public getMovies(page: number): Observable<IGetMoviesResponse> {
    let params = new HttpParams();
    params = params.append('page', page + '');

    return this.http.get<IGetMoviesResponse>(this.MOVIES_URL, {params})
      .pipe(
        map((response: IGetMoviesResponse) => {
          return response;
        })
      );
  }

  public getSuggestedMovies(page: number): Observable<IGetMoviesResponse> {
    let params = new HttpParams();
    params = params.append('page', page + '');

    return this.http.get<IGetMoviesResponse>(this.SUGGESTED_MOVIES_URL, {
      params,
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + MoviesApiService.getToken()),
    })
      .pipe(
        map((response: IGetMoviesResponse) => {
          return response;
        })
      );
  }

  public getMyMovies(page: number): Observable<IGetMoviesResponse> {
    let params = new HttpParams();
    params = params.append('page', page + '');

    return this.http.get<IGetMoviesResponse>(this.MY_MOVIES_URL, {
      params,
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + MoviesApiService.getToken()),
    })
      .pipe(
        map((response: IGetMoviesResponse) => {
          return response;
        })
      );
  }

  public createMovie(formData: FormData): Observable<boolean> {
    return this.http.post(this.MY_MOVIES_URL, formData, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + MoviesApiService.getToken()),
    }).pipe(
      map((response: any) => {
        return true;
      })
    );
  }

  public getMovieDetails(slug: string): Observable<IMovieDetails> {
    return this.http.get<IMovieDetails>(this.MOVIES_URL + '/' + slug)
      .pipe(
        map((response: IMovieDetails) => {
          return response;
        })
      );
  }

  public deleteMovie(slug: string): Observable<boolean> {
    return this.http.delete(this.DELETE_MOVIE_URL + '/' + slug, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + MoviesApiService.getToken()),
    }).pipe(
      map((response: any) => {
        console.log(response)
        return true;
      })
    );
  }

  public signUp(signUpRequest: ISignUpRequest): Observable<ISignUpResponse> {
    return this.http.post(this.SIGN_UP_URL, signUpRequest)
      .pipe(
        map((response: ISignUpResponse) => {
          return response;
        })
      );
  }

  public logOut(): Observable<boolean> {
    return this.http.get(this.LOG_OUT_URL, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + MoviesApiService.getToken()),
    })
      .pipe(
        map((response: any) => {
          return response.response;
        })
      );
  }

  public logIn(email: string, password: string): Observable<boolean> {
    const body = {
      username: email,
      password
    };
    return this.http.post(this.LOG_IN_URL, body)
      .pipe(
        map((response: any) => {
          const token = response.token;
          if (token && token !== '') {
            MoviesApiService.saveToken(token);
            return true;
          }
          return false;
        })
      );
  }
}
